#ifndef RDI_MATCH_KALDI_SUBVECTORS_H
#define RDI_MATCH_KALDI_SUBVECTORS_H

#include <string>
#include <matrix/kaldi-matrix.h>
#include <cmath>
#include <sstream>

#include <catch.hpp>

namespace RDI
{

/// A Catch.hpp matcher for kaldi::SubVector<kaldi::BaseFloat>
/// With EPSILON as the upper bound on the relative error
/// Kaldi is a toolkit for speech recognition written in C++
/// https://github.com/kaldi-asr/kaldi
class MatchKaldiSubVectors : public Catch::MatcherBase<kaldi::SubVector<kaldi::BaseFloat>>
{
	kaldi::SubVector<kaldi::BaseFloat> m_me;
	kaldi::BaseFloat m_epsilon;

public:
	MatchKaldiSubVectors(const kaldi::SubVector<kaldi::BaseFloat>& me, kaldi::BaseFloat epsilon = 0.01f) :
		m_me(me), m_epsilon(epsilon) {}

	bool AreSame(kaldi::BaseFloat a, kaldi::BaseFloat b) const
	{
		return std::fabs(a - b) < m_epsilon;
	}

	virtual bool match(const kaldi::SubVector<kaldi::BaseFloat>& other) const override
	{
		if(m_me.Dim() != other.Dim())
		{
			return false;
		}

		for(int i = 0; i < m_me.Dim(); i++)
		{
			if(!AreSame(other(i), m_me(i)))
			{
				return false;
			}
		}

		return true;
	}

	virtual std::string describe() const override
	{
		std::ostringstream ss;
		ss << "==\n{ ";
		bool first = true;
		for(int i = 0; i < m_me.Dim(); i++)
		{
			if (first)
			{
				first = false;
			}
			else
			{
				ss << ", ";
			}

			ss << std::to_string(m_me(i));
		}

		ss << " }\n";
		return ss.str();
	}
};

// The builder function
template <typename Real,
          typename =
          typename std::enable_if_t<std::is_floating_point<Real>::value>>

inline MatchKaldiSubVectors IsEqualTo( kaldi::SubVector<Real> other,
                                       Real epsilon = 0.01)
{
	return MatchKaldiSubVectors( other,epsilon );
}

} // namespace RDI

#endif // RDI_MATCH_KALDI_SUBVECTORS_H
