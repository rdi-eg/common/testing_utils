TARGET = testing_utils
TEMPLATE = lib
CONFIG += staticlib c++1z
QMAKE_CXXFLAGS += -std=c++17

INCLUDEPATH += /opt/rdi/include
INCLUDEPATH += /opt/kaldi/src
INCLUDEPATH += /opt/kaldi/tools/openfst/include
INCLUDEPATH += /opt/kaldi/tools/include

HEADERS += \
    rdi_testing_utils.hpp \
    rdi_match_std_vectors.hpp \
    rdi_match_kaldi_subvectors.hpp

