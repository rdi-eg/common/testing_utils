#include <string>
#include <vector>
#include <unistd.h>

#include <rdi_wfile.hpp>
#include <rdi_stl_utils.hpp>

namespace RDI
{

inline float
get_mem_usage()
{
	std::string stat = read_file("/proc/self/stat");
	std::vector<std::string> tokens = split(stat);
	float page_size_kb = sysconf(_SC_PAGE_SIZE) / 1048576.0f ; // in case x86-64 is configured to use 2MB pages
	return std::stof(tokens[23]) * page_size_kb;
}

} // namespace RDI
