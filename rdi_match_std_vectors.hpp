#ifndef RDI_MATCH_VECTORS_H
#define RDI_MATCH_VECTORS_H

#include <string>
#include <vector>
#include <cmath>
#include <sstream>

#include <catch.hpp>

namespace RDI
{

/// A Catch.hpp matcher for vector<float> or vector<double>
/// With EPSILON as the upper bound on the relative error
/// Note: if you need a std::vector<int> or any other arithmatic type
/// you could simply use the catch built in matcher and it would work just
/// fine
template <typename Real,
		  typename =
		  typename std::enable_if_t<std::is_floating_point<Real>::value>>
class MatchRealVectors : public Catch::MatcherBase<std::vector<Real>>
{
	std::vector<Real> m_me;
	Real m_epsilon;

public:
	MatchRealVectors(const std::vector<Real>& me, Real epsilon = 0.01) :
		m_me(me), m_epsilon(epsilon) {}

	bool AreSame(Real a, Real b) const
	{
		return std::fabs(a - b) < m_epsilon;
	}

	virtual bool match(const std::vector<Real>& other) const override
	{
		if(m_me.size() != other.size())
		{
			return false;
		}

		for(size_t i = 0; i < m_me.size(); i++)
		{
			if(!AreSame(other[i], m_me[i]))
			{
				return false;
			}
		}

		return true;
	}

	virtual std::string describe() const
	{
		std::ostringstream ss;
		ss << "==\n{ ";
		bool first = true;
		for(size_t i = 0; i < m_me.size(); i++)
		{
			if (first)
			{
				first = false;
			}
			else
			{
				ss << ", ";
			}

			ss << std::to_string(m_me[i]);
		}

		ss << " }\n";
		return ss.str();
	}
};

// The builder function
template <typename Real,
		  typename =
		  typename std::enable_if_t<std::is_floating_point<Real>::value>>
inline MatchRealVectors<Real> IsEqualTo(std::vector<Real> other,
										Real epsilon = 0.01)
{
	return MatchRealVectors<Real>(other, epsilon);
}

} // namespace RDI

#endif // RDI_MATCH_VECTORS_H
